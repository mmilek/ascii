#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char* argv[]){

	char* ascii[129];
	{
		ascii[0] = "NULL";
		ascii[1] = "START OF HEADING";
		ascii[2] = "START OF TEXT";
		ascii[3] = "END OF TEXT";
		ascii[4] = "END OF TRANSMISSION";
		ascii[5] = "ENQUIRY";
		ascii[6] = "ACKNOWLEDGE";
		ascii[7] = "BELL";
		ascii[8] = "BACKSPACE";
		ascii[9] = "HORIZONTAL TAB";
		ascii[10] = "LINE FEED";
		ascii[11] = "VERTICAL TAB";
		ascii[12] = "FORM FEED";
		ascii[13] = "CARRIAGE RETURN";
		ascii[14] = "SHIFT OUT";
		ascii[15] = "SHIFT IN";
		ascii[16] = "DATA LINK ESCAPE";
		ascii[17] = "DEVICE CONTROL 1 (XON)";
		ascii[18] = "DEVICE CONTROL 2";
		ascii[19] = "DEVICE CONTROL 3 (XOFF)";
		ascii[20] = "DEVICE CONTROL 4";
		ascii[21] = "NEGATIVE ACKNOWLEDGE";
		ascii[22] = "SYNCHRONOUS IDLE";
		ascii[23] = "END OF TRANSMISSION BLOCK";
		ascii[24] = "CANCEL";
		ascii[25] = "END OF MEDIUM";
		ascii[26] = "SUBSTITUTE";
		ascii[27] = "ESCAPE";
		ascii[28] = "FILE SEPARATOR";
		ascii[29] = "GROUP SEPARATOR";
		ascii[30] = "RECORD SEPARATOR";
		ascii[31] = "UNIT SEPARATOR";
		ascii[32] = "SPACE";
		ascii[127] = "DELETE";
	}

	for (int i=33; i<=126; i++) {
		ascii[i] = (char*)malloc(1);
		ascii[i][0] = i;
	}

	if (argc == 1) {
		for (int i=0; i<=42; i++) {
			if (i<10) {
				printf("%d: %-35s %d: %-35s%d: %-35s\n", i, ascii[i], i+42, ascii[i+42], i+42+42, ascii[i+42+42]);
			}
			else {
				printf("%d: %-35s%d: %-35s%d: %-35s\n", i, ascii[i], i+42, ascii[i+42], i+42+42, ascii[i+42+42]);
			}
		}
		printf("127: %s\n", ascii[127]);
	}

	else if (argc == 2 && (strcmp(argv[1], "-l") == 0 || strcmp(argv[1], "--long") == 0)) {
		for (int i=0; i<=127; i++) {
			printf("%d: '%s'\n", i, ascii[i]);
		}
	}

	else if (argc == 3 && (strcmp(argv[1], "-n") == 0 || strcmp(argv[1], "--number") == 0)) {
		int num = atoi(argv[2]);
		if (num > 0 && num < 128) {
			printf("%d: '%s'", num, ascii[num]);
		}
		else {
			printf("Invalid argument\n");
			for (int i=33; i<=126; i++) {
				free(ascii[i]);
			}
			return EXIT_FAILURE;
		}
	}

	else if (argc == 3 && (strcmp(argv[1], "-c") == 0 || strcmp(argv[1], "--character") == 0)) {
		for (int i=0; i<strlen(argv[2]); i++) {
			printf("'%c': %d\n", argv[2][i], argv[2][i]);
		}
	}

	else if (argc == 2 && (strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "--help") == 0)) {
		puts("NAME\nascii - an ASCII cheatsheet\n\nUSAGE:\nascii [-c/-n/-h] [character/number]\n\nOPTIONS:\n-c/--character  outputs an ASCII code corresponding to the entered character. In case a string is entered, it will output the code of every letter in that string");
		puts("-n/--number  output a character corresponding to the entered ASCII code in decimal\n-h/--help  print help\n");
		puts("AUTHOR\nMichał Miłek\n2022\n");
		puts("LICENSING\nascii is published under the GNU GPLv3 license");
	}
	
	else {
		puts("Invalid arguments\nsee -h/--help");
		for (int i=33; i<=126; i++) {
			free(ascii[i]);
		}
		return EXIT_FAILURE;
	}

	for (int i=33; i<=126; i++) {
		free(ascii[i]);
	}

	return EXIT_SUCCESS;
}
